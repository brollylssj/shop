from django.shortcuts import get_object_or_404, redirect, render
from django.views import View
from django.views.generic import TemplateView

from coupons.forms import CouponForm
from shop_backet import Cart
from forms import CartAddProductForm
from shop.models import Product


class CartAddView(View):
    form_class = CartAddProductForm

    def post(self, request, product_id):
        cart = Cart(request)
        product = get_object_or_404(Product, id=product_id)
        form = self.form_class(request.POST)
        if form.is_valid():
            form_cd = form.cleaned_data
            cart.add(product=product, quantity=form_cd['quantity'], update_quantity=form_cd['update'])
        return redirect('cart:cart_detail')


class CartRemoveView(View):
    def get(self, request, product_id):
        cart = Cart(request)
        product = get_object_or_404(Product, id=product_id)
        cart.remove(product)
        return redirect('cart:cart_detail')


class CartDetailView(TemplateView):
    template_name = 'cart/detail.html'

    def get(self, request, *args, **kwargs):
        cart = Cart(request)
        for item in cart:
            item['update_quantity_form'] = CartAddProductForm(
                initial={'quantity': item['quantity'],
                         'update': True})
        coupon_apply_form = CouponForm()
        return render(request, self.template_name, {'cart': cart, 'coupon_apply_form': coupon_apply_form})
