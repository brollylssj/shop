from django.conf.urls import url

from shop.views import ProductListView, ProductDetailView

urlpatterns = [
    url(r'^$', ProductListView.as_view(), name='product_list'),
    url(r'^(?P<category_slug>[-\w]+)/$', ProductListView.as_view(), name='product_list_by_category'),
    url(r'^(?P<id>\d+)/(?P<slug>[-\w]+)/$', ProductDetailView.as_view(), name='product_detail'),
]
