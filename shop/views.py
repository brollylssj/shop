from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView

from cart.forms import CartAddProductForm
from shop.models import Category, Product


class ProductListView(TemplateView):
    template_name = 'shop/product/list.html'

    def get(self, request, category_slug=None):
        category = None
        categories = Category.objects.all()
        products = Product.objects.filter(available=True)
        if category_slug:
            category = get_object_or_404(Category, slug=category_slug)
            products = products.filter(category=category)
        context = {
            'category': category,
            'categories': categories,
            'products': products
        }
        return render(request, self.template_name, context)


class ProductDetailView(TemplateView):
    template_name = 'shop/product/detail.html'

    def get(self, request, id, slug):
        product = get_object_or_404(Product, id=id, slug=slug, available=True)
        cart_product_form = CartAddProductForm()
        return render(request, self.template_name, {'product': product, 'cart_product_form': cart_product_form})
