#-*- coding: utf-8 -*-
import csv

import datetime

import weasyprint
from django.conf import settings
from django.contrib import admin
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from orders.models import OrderItem, Order


def export_to_csv(modeladmin, request, queryset):
    opts = modeladmin.model._meta
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment filename={}.csv'.format(opts.verbose_name)
    writer = csv.writer(response)
    fields = [field for field in opts.get_fields() if not field.many_to_many and not field.one_to_many]
    writer.writerow([field.verbose_name for field in fields])
    for obj in queryset:
        data_row = []
        for field in fields:
            value = getattr(obj, field.name)
            if isinstance(value, datetime.datetime):
                value = value.strftime('%d/%m/%Y')
            data_row.append(value)
        writer.writerow([unicode(s).encode("utf-8") for s in data_row])
    return response

export_to_csv.short_description = 'Eksport to CSV'


def order_detail(obj):
    return '<a href="{}">Wyświetl</a>'.format(
        reverse('orders:admin_order_detail', args=[obj.id]))
order_detail.allow_tags = True


def order_pdf(obj):
    return '<a href="{}">PDF</a>'.format(
        reverse('orders:admin_order_pdf', args=[obj.id]))
order_pdf.allow_tags = True
order_pdf.short_description = 'Rachunek PDF'


class AdminOrderPDFView(TemplateView):
    template_name = 'orders/order/pdf.html'

    @method_decorator(staff_member_required)
    def get(self, request, order_id):
        order = get_object_or_404(Order, id=order_id)
        html = render_to_string(self.template_name, {'order': order})
        response = HttpResponse(content_type='aplication/pdf')
        response['Content-Disposition'] = 'filename="order_{}.pdf"'.format(order_id)
        weasyprint.HTML(string=html).write_pdf(response,
                                               stylesheets=[weasyprint.CSS(settings.STATIC_ROOT + 'css/pdf.css')])
        return response


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ['product']


class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'first_name', 'last_name', 'email', 'address', 'postal_code',
                    'city', 'paid', 'created', 'updated', order_detail, order_pdf]
    list_filter = ['paid', 'created', 'updated']
    inlines = [OrderItemInline]
    actions = [export_to_csv]


class AdminOrderDetailView(TemplateView):
    template_name = 'admin/orders/order/detail.html'

    @method_decorator(staff_member_required)
    def get(self, request, order_id):
        order = get_object_or_404(Order, id=order_id)
        return render(request, self.template_name, {'order': order})

admin.site.register(Order, OrderAdmin)
