# -*- coding: utf-8 -*-
from celery.task import task
from django.core.mail import send_mail

from orders.models import Order


@task
def order_created(order_id):
    order = Order.objects.get(id=order_id)
    subject = 'nr {} '.format(order.id)
    message = 'Witaj, {}!\n\nZłozyles zamowienie w naszym sklepie.'.format(order.first_name)
    mail_sent = send_mail(subject, message, 'shop@shop.com', [order.email])
    return mail_sent
