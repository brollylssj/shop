#-*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import TemplateView

from cart.shop_backet import Cart
from orders.forms import OrderCreateForm
from orders.models import OrderItem
from .tasks import order_created


class CreateOrderView(TemplateView):
    template_name_create = 'orders/order/create.html'
    template_name_created = 'orders/order/created.html'
    class_form = OrderCreateForm

    def get(self, request, *args, **kwargs):
        cart = Cart(request)
        context = {
            'cart': cart,
            'form': self.class_form(),
        }
        return render(request, self.template_name_create, context)

    def post(self, request, *args, **kwargs):
        cart = Cart(request)
        form = self.class_form(request.POST)
        if form.is_valid():
            order = form.save(commit=False)
            if cart.coupon:
                order.coupon = cart.coupon
                order.discount = cart.coupon.discount
            order.save()
            for item in cart:
                OrderItem.objects.create(order=order, product=item['product'],
                                         price=item['price'], quantity=item['quantity'])
            cart.clear()
            order_created(order.id)
            request.session['order_id'] = order.id
            return redirect(reverse('payment:process'))
