from django.conf.urls import url

from coupons.views import CouponApplyView

urlpatterns = [
    url(r'^apply/$', CouponApplyView.as_view(), name='apply'),
]